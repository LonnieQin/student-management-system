//
//  main.c
//  StudentGradeManageMent
//
//  Created by apple on 16/2/20.
//  Copyright © 2016年 apple. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cJSON.h"
#pragma mark - 宏
#define FILENAME "myclass.txt"
#define LINE printf("\n");
#pragma mark - 结构体

//课程
typedef struct {
    char * name;
} Course;

//学生
typedef struct {
    char * name;
    int number;
    float scores[6];
} Student;
typedef enum {
    //根据成绩升序排行
    ScoreAcending,
    //根据成绩降序排行
    ScoreDescending,
    //根据学号排行
    Number,
    //根据姓名排行
    Name,
} SortBy;
//班级
typedef struct {
    char * className;
    int studentNum;
    int courseNum;
    Student students[30];
    Course courses[6];
} Class;

static Class myClass;

#pragma mark - Class
//Class from input
Class ClassFromInput()
{
    Class class;
    printf("Now You Create a class:\n");
    printf("Your class name:");
    
    class.className = (char*)malloc(100);
    char className[100];
    scanf("%s",className);
    strcpy(class.className, className);
    LINE
    printf("Count of Student:");
    scanf("%d",&class.studentNum);
    LINE
    printf("Count of Course:");
    while (1) {
        scanf("%d",&class.courseNum);
        if (class.courseNum <= 0 && class.courseNum > 6) {
            LINE
            printf("Your course number is not right.please input again!");
            LINE
        } else {
            break;
        }
    }
    LINE
    for (int i = 0;  i < class.courseNum; ++i) {
        Course course;
        printf("%dth course name:",i+1);
        course.name = malloc(100);
        scanf("%s",course.name);
        class.courses[i] = course;
    }
    return class;
}


/*把把班级的结构体转为JSON字符串存入文件，格式如下
 {
	"className":	"202",
	"studentNum":	3,
	"courseNum":	2,
	"students":	[{
 "name":	"AAA",
 "number":	1,
 "scores":	[3, 99]
 }, {
 "name":	"BBB",
 "number":	2,
 "scores":	[88, 88]
 }, {
 "name":	"CCC",
 "number":	3,
 "scores":	[33, 99]
 }],
	"courses":	[{
 "name":	"Chinese"
 }, {
 "name":	"Math"
 }]
 }
 */
void ClassSaveToFile(Class cls,char * fileName)
{
    if (isClassHaveValue(cls)) {
        cJSON * class = cJSON_CreateObject();
        cJSON_AddStringToObject(class, "className", cls.className);
        cJSON_AddNumberToObject(class, "studentNum", cls.studentNum);
        cJSON_AddNumberToObject(class, "courseNum", cls.courseNum);
        cJSON * students = cJSON_CreateArray();
        for (int i = 0; i < cls.studentNum; ++i) {
            Student student = cls.students[i];
            cJSON * item = cJSON_CreateObject();
            cJSON_AddStringToObject(item, "name", student.name);
            cJSON_AddNumberToObject(item, "number", student.number);
            cJSON * scores = cJSON_CreateFloatArray(student.scores, myClass.courseNum);
            cJSON_AddItemToObject(item, "scores", scores);
            cJSON_AddItemToArray(students, item);
        }
        cJSON * courses = cJSON_CreateArray();
        for (int i = 0; i < cls.courseNum; ++i) {
            Course course = cls.courses[i];
            cJSON * item = cJSON_CreateObject();
            cJSON_AddStringToObject(item, "name", course.name);
            cJSON_AddItemToArray(courses, item);
        }
        cJSON_AddItemToObject(class, "students", students);
        cJSON_AddItemToObject(class, "courses", courses);
        char * jsonString = cJSON_Print(class);
        //printf("josn string:%s\n",jsonString);
        size_t fileLen = strlen(jsonString);
        
        FILE * fp;
        if ((fp = fopen(fileName, "wb")) == NULL) {
            exit(0);
        }
        rewind(fp);
        fwrite(jsonString, fileLen, sizeof(char), fp);
        fclose(fp);
        printf("Wirte to File Succeed!\n");
    } else {
        printf("The class is null!\n");
    }
}


//通过文件还原班级
Class ClassFromFile(char * fileName)
{
    FILE * fp;
    if ((fp = fopen(fileName, "r")) == NULL) {
        printf("The File is Not exist!\n");
        exit(0);
    }
    fseek(fp, 0, SEEK_END);
    size_t fileLen = ftell(fp);
    char * jsonString = (char*) malloc(sizeof(char)*fileLen);
    fseek(fp, 0, SEEK_SET);
    fread(jsonString, fileLen, sizeof(char), fp);
    fclose(fp);
    //printf("JSON String:%s",jsonString);
    
    cJSON * class = cJSON_Parse(jsonString);
    
    
    Class c;
    c.className = cJSON_GetObjectItem(class, "className")->valuestring;
    c.studentNum = cJSON_GetObjectItem(class, "studentNum")->valueint;
    c.courseNum = cJSON_GetObjectItem(class, "courseNum")->valueint;
    
    cJSON * students = cJSON_GetObjectItem(class, "students");
    cJSON * courses = cJSON_GetObjectItem(class, "courses");
    for (int i = 0; i < c.studentNum; ++i) {
        Student student;
        cJSON * item = cJSON_GetArrayItem(students, i);
        student.name = cJSON_GetObjectItem(item, "name")->valuestring;
        student.number = cJSON_GetObjectItem(item, "number")->valueint;
        cJSON * scores = cJSON_GetObjectItem(item, "scores");
        for (int j = 0; j < c.courseNum; ++j) {
            cJSON * item2 = cJSON_GetArrayItem(scores, j);
            student.scores[j] = item2->valuedouble;
        }
        c.students[i] = student;
    }
    
    for (int i = 0; i < c.courseNum; ++i) {
        Course course;
        cJSON * item = cJSON_GetArrayItem(courses, i);
        course.name = cJSON_GetObjectItem(item, "name")->valuestring;
        c.courses[i] = course;
    }
    return c;
}


//班级是否有数据
int isClassHaveValue(Class class) {
    return (class.className != NULL && class.studentNum > 0 && class.courseNum > 0);
}



#pragma mark - Functions
//Print Menu
void PrintMenu()
{
    printf("1.Input Record\n");
    printf("2.Caculate total and average score of every course\n");
    printf("3.Caculate total and average score of every student\n");
    printf("4.Sort in descending order by total score of every student\n");
    printf("5.Sort in ascending ordr by total socre of every student\n");
    printf("6.Sort in ascending order by number\n");
    printf("7.Sort in dictionary order by name\n");
    printf("8.Search by number\n");
    printf("9.Search by name\n");
    printf("10.Statistic analysis for every coursee\n");
    printf("11.List Record\n");
    printf("12.Write To a file\n");
    printf("13.Read from a file\n");
    printf("0.Exit\n");
}

//1.input record
void InputRecord()
{
    //如果班级名为空，输入新的班级
    if (!isClassHaveValue(myClass)) {
        printf("Please input your class infomation first.");
        LINE
        myClass = ClassFromInput();
    }
    for (int i = 0; i < myClass.studentNum; ++i) {
        Student student;
        //输入学号
        printf("Student Number:");
        scanf("%d",&student.number);
        LINE
        //输入姓名
        char c2[100];
        printf("Student Name:");
        student.name= (char*)malloc(100);
        scanf("%s",c2);
        strcpy(student.name, c2);
        LINE
        //各科考试成绩
        for (int j = 0; j < myClass.courseNum; ++j) {
            Course course = myClass.courses[j];
            printf("%s Grade:",course.name);
            float score = 0;
            scanf("%f",&score);
            student.scores[j] = score;
            LINE
        }
        myClass.students[i] = student;
    }
    
}

//2.Caculate total and average score of every course
void CaculateTotalAndAverageScoreOfEveryCourse()
{
    if (isClassHaveValue(myClass)) {
        for (int i = 0; i < myClass.courseNum; ++i) {
            float totalScore = 0;
            float averageScore = 0;
            Course course = myClass.courses[i];
            for (int j = 0; j < myClass.studentNum; ++j) {
                Student student = myClass.students[j];
                totalScore += student.scores[i];
            }
            averageScore = (float)totalScore / (float)myClass.studentNum;
            printf("Total score of %s is %.2f,average score of %s is %.2f.\n",course.name,totalScore,course.name,averageScore);
        }
    }
}
//3.Caculate total and average score of every student
void CaculateTotalAndAverageScoreOfEveryStudent()
{
    if (isClassHaveValue(myClass)) {
        for (int j = 0; j < myClass.studentNum; ++j) {
            float totalScore = 0;
            float averageScore = 0;
            Student student = myClass.students[j];
            for (int i = 0; i < myClass.courseNum; ++i) {
                totalScore += student.scores[i];
            }
            averageScore = (float)totalScore / (float)myClass.courseNum;
            printf("Total score of %s is %.2f,average score of %s is %.2f.\n",student.name,totalScore,student.name,averageScore);
        }
    }
}

//4.5. 通过总分排序


int CompareByStudentScoreAcending(const void * a,const void * b);
int CompareByStudentScoreDescending(const void * a,const void * b);
int CompareByName(const void * a,const void * b);
int CompareByNumber(const void * a,const void * b);
void SortStudentBy(SortBy sortBy)
{
    switch (sortBy) {
        case ScoreAcending:
            qsort(myClass.students, myClass.studentNum, sizeof(Student),CompareByStudentScoreAcending);
            break;
        case ScoreDescending:
            qsort(myClass.students, myClass.studentNum, sizeof(Student),CompareByStudentScoreDescending);
            break;
        case Name:
            qsort(myClass.students, myClass.studentNum, sizeof(Student), CompareByName);
            break;
        case Number:
            qsort(myClass.students, myClass.studentNum, sizeof(Student), CompareByNumber);
            return;
        default:
            break;
    }
}

#pragma mark - 比较函数
//根据成绩升序排行比较
int CompareByStudentScoreAcending(const void * a,const void * b){
    Student * c = (Student*)a;
    Student * d = (Student*)b;
    float scoreOfA = 0;
    float scoreOfB = 0;
    for (int i = 0; i < myClass.courseNum; ++i) {
        scoreOfA += c->scores[i];
        scoreOfB += d->scores[i];
    }
    if (scoreOfA > scoreOfB) return 1;
    else if (scoreOfA < scoreOfB) return -1;
    return 0;
}

//根据成绩降序比较
int CompareByStudentScoreDescending(const void * a,const void * b){
    
    return -CompareByStudentScoreAcending(a, b);
}

//根据学号比较
int CompareByNumber(const void * a,const void * b){
    Student * c = (Student*)a;
    Student * d = (Student*)b;
    if (c->number > d->number) {
        return 1;
    }
    if (c->number < d->number) {
        return -1;
    }
    return 0;
}

//根据姓名比较
int CompareByName(const void * a,const void * b)
{
    Student * c = (Student*)a;
    Student * d = (Student*)b;
    //升序
    return strcmp(c->name, d->name);
}


void SearchByName()
{
    SortStudentBy(ScoreDescending);
    char * s = (char*)malloc(100);
    char c[100];
    printf("Student Name:");
    scanf("%s",c);
    strcpy(s, c);
    LINE
    int index = -1;
    for (int i = 0; i < myClass.studentNum; ++i) {
        Student student = myClass.students[i];
        if (strcmp(s, student.name) == 0) {
            index = i;
            break;
        }
    }
    if (index >= 0) {
        Student student = myClass.students[index];
        printf("name:%s\n",student.name);
        printf("num:%d\n",student.number);
        printf("Rank:%d\n",index);
        for (int j = 0; j < myClass.courseNum; ++j) {
            printf("%s Score:%f\n",myClass.courses[j].name,student.scores[j]);
        }
    } else {
        printf("Student is not exist.\n");
    }
}

void SearchByNumber()
{
    SortStudentBy(ScoreDescending);
    
    int number = 0;
    printf("Student Number:");
    scanf("%d",&number);
    LINE
    int index = -1;
    for (int i = 0; i < myClass.studentNum; ++i) {
        Student student = myClass.students[i];
        if (number == student.number) {
            index = i;
            break;
        }
    }
    if (index >= 0) {
        Student student = myClass.students[index];
        printf("Name:%s\n",student.name);
        printf("Num:%d\n",student.number);
        printf("Rank:%d\n",index);
        for (int j = 0; j < myClass.courseNum; ++j) {
            printf("%s Score:%f\n",myClass.courses[j].name,student.scores[j]);
        }
    } else {
        printf("Student is not exist.\n");
    }
}

void StatisticAnalysisForEveryCourse()
{
    if (isClassHaveValue(myClass)) {
        for (int i = 0; i < myClass.courseNum; ++i) {
            Course course = myClass.courses[i];
            int verygood = 0;
            int good = 0;
            int medium = 0;
            int pass = 0;
            int notpass = 0;
            for (int j = 0; j < myClass.studentNum; ++j) {
                Student student = myClass.students[j];
                float score = student.scores[i];
                if (score <= 100 && score >= 90)verygood++;
                else if (score <= 89 && score >= 80) good++;
                else if (score <= 79 && score >= 70) medium++;
                else if (score <= 69 && score >= 60) pass++;
                else if (score < 60)notpass++;
            }
            printf("%s\n",course.name);
            printf("Very Good:%d %.2f%%\n",verygood,100*verygood/(float)myClass.studentNum);
            printf("Good:%d %.2f%%\n",good,100*good/(float)myClass.studentNum);
            printf("Medium:%d %.2f%%\n",medium,100*medium/(float)myClass.studentNum);
            printf("Pass:%d %.2f%%\n",pass,100*pass/(float)myClass.studentNum);
            printf("Not Pass:%d %.2f%%\n",notpass,100*notpass/(float)myClass.studentNum);
        }
    }
}

void ListRecord()
{
    if (isClassHaveValue(myClass)) {
        for (int i = 0; i < myClass.studentNum; ++i) {
            Student student = myClass.students[i];
            printf("Name:%s\n",student.name);
            printf("Num:%d\n",student.number);
            for (int j = 0; j < myClass.courseNum; ++j) {
                printf("%s Score:%f\n",myClass.courses[j].name,student.scores[j]);
            }
        }
        CaculateTotalAndAverageScoreOfEveryCourse();
    }
}


void WriteToAFile()
{
    ClassSaveToFile(myClass, FILENAME);
}

void ReadFromAFile()
{
    myClass = ClassFromFile(FILENAME);
}

//打印名次表
void PrintLegueTable()
{
    printf("Student league table:\n");
    for (int i = 0; i < myClass.studentNum;++i) {
        float totalScore = 0;
        for (int j = 0; j < myClass.courseNum; ++j) {
            totalScore += myClass.students[i].scores[j];
        }
        printf("%d th:%s %.0f\n",i+1,myClass.students[i].name,totalScore);
    }
}
//打印成绩表
void PrintScoreTable()
{
    printf("Student score table:\n");
    for (int i = 0; i < myClass.studentNum;++i) {
        Student student = myClass.students[i];
        printf("%d %s\n",student.number,student.name);
        for (int j = 0; j < myClass.courseNum; ++j) {
            printf("%s:%f\n",myClass.courses[j].name,student.scores[j]);
        }
    }
}

int main(int argc, const char * argv[]) {
    int shouldContinue = 1;
    while (shouldContinue) {
        PrintMenu();
        int choise;
        scanf("%d",&choise);
        switch (choise) {
            case 1:
                InputRecord();
                break;
            case 2:
                CaculateTotalAndAverageScoreOfEveryCourse();
                break;
            case 3:
                CaculateTotalAndAverageScoreOfEveryStudent();
                break;
            case 4:
                SortStudentBy(ScoreDescending);
                PrintLegueTable();
                break;
            case 5:
                SortStudentBy(ScoreAcending);
                PrintLegueTable();
                break;
            case 6:
                SortStudentBy(Number);
                PrintScoreTable();
                break;
            case 7:
                SortStudentBy(Name);
                PrintScoreTable();
                break;
            case 8:
                SearchByNumber();
                break;
            case 9:
                SearchByName();
                break;
            case 10:
                StatisticAnalysisForEveryCourse();
                break;
            case 11:
                ListRecord();
                break;
            case 12:
                WriteToAFile();
                break;
            case 13:
                ReadFromAFile();
                break;
            case 0:
                shouldContinue = 0;
                break;
            default:
                break;
        }
        if(!shouldContinue)break;
        printf("Press Enter to Continue...\n");
        getchar();
        getchar();
        
    }
    return 0;
}
